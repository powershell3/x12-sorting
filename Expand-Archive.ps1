<#

.SYNOPSIS
    Built as a workaround to wanting powershell parms on the main script but not having 'Expand-Archive' in PS v2. 
    This will run and trigger X12TSort.ps1 on 03 to sort after files have been unzipped.

.DESCRIPTION
    Unzips files and removes the original .zip file.

.POWERSHELL_VERSION 
	V2
	V5

.TABLES
	N/A

.INPUT_FILES
	N/A

.OUTPUT_FILES
	N/A

.RERUN_INSTRUCTIONS
	Can be re-run freely.

.REVISION
	Name: Scott VanderHorst
	Date: 5/7/2020
	Description: Original Code
    
.KNOWN_ISSUES
    N/A

.#>
param(
    [string] $jobNumber = "",
    [string] $sourceDir
);
Function isZipped {
    $allFiles = Get-ChildItem "$sourceDir\*"
    if ($allFiles) {
        Foreach ($file in $allFiles) {
            $extension = [System.IO.Path]::GetExtension($file)
            if ($extension -eq ".zip") {
                Expand-Archive $file $sourceDir
                Remove-Item $file
                Write-Host "$file unzipped, original removed."
            }
        }
    } else {
        Write-Host "No files to unzip in $sourceDir, calling it a day."
    }
}

isZipped
