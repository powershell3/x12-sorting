<#

.SYNOPSIS
    This job runs on a scheduled basis to sort X12 files from Axiom to their proper FTP directories

.DESCRIPTION
    Sorts based on ISA06/ISA08 entries. First run them through a function to find any with non-standard ISA06 entries

.POWERSHELL_VERSION 
	V2
	V5

.TABLES
	N/A

.INPUT_FILES
	N/A

.OUTPUT_FILES
	N/A

.RERUN_INSTRUCTIONS
	Can be re-run freely.

.REVISION
	Name: Scott VanderHorst
	Date: 4/22/2020
	Description: Original Code
    
.KNOWN_ISSUES
    N/A

.#>
param(
    [string] $jobNumber = "",
    [string] $Direction
);

$includePath = "$(Split-Path -Path $MyInvocation.MyCommand.Path -Parent | Split-Path -Parent)\include"
. $includePath\utils.ps1

if ($null -eq $Direction -or $Direction -eq "") {
    throw "Direction parameter necessary to determine input and output paths. Please pass -Direction 'in' or 'out'"
};
if ($Direction -ne "In" -and $Direction -ne "Out") {
    throw "Direction can only be 'in' or 'out'. Please enter either variant"
}

Run-Job -JobName 'X12SSort' {
    $ISAHash = @{ }
    $ISAValues.Split(';') | ForEach-Object { 
        $key, $value = $_.split(',')
        $ISAHash[$key] = $value    
    }
    Function NonStandardISA {
        $allFiles = Get-ChildItem "$sourceDir\$Direction\*"
        if ($allFiles) {
            If ($direction -eq "In") {
                Foreach ($file in $allFiles) {
                    $ISARead = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 50) -join '')
                    $siteID = $ISARead.Substring(35, 15)
    
                    foreach ($key in $ISAHash.Keys.GetEnumerator()) {
                        switch ($siteID) {
                            $key {
                                Write-host "'$_' is in the non-standard ISA table"
                                $value = $ISAHash.GetEnumerator() | Where-Object { $_.Key -eq $siteID } | ForEach-Object { $_.Value }
                                Write-Host "'$_' translates to $value"
                                Move-Item -literalpath $file "$destroot\$direction\$value"
                                Write-host "'$file' moved to '$destroot\$direction\$value'"
                                break
                            }
                            default { 
                                break 
                            }
                        }
                    }
                }
            }
            else {
                Foreach ($file in $allFiles) {
                    $ISARead = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 70) -join '')
                    $siteID = $ISARead.Substring(54, 15)
    
                    foreach ($key in $ISAHash.Keys.GetEnumerator()) {
                        switch ($siteID) {
                            $key {
                                Write-host "'$_' is in the non-standard ISA table"
                                $value = $ISAHash.GetEnumerator() | Where-Object { $_.Key -eq $siteID } | ForEach-Object { $_.Value }
                                Write-Host "'$_' translates to $value"
                                Move-Item -literalpath $file "$destroot\$direction\$value"
                                Write-host "'$file' moved to '$destroot\$direction\$value'"
                                break
                            }
                            default { 
                                break 
                            }
                        }
                    }
                }
            }
        }
        else {
            Write-Host "No files, moving on"
        }
    }
    Function StandardISA {
        $allFiles = Get-ChildItem "$sourceDir\$Direction\*"
        if ($allFiles) {
            if ($Direction -eq "In") {
                Foreach ($file in $allFiles) {
                    $ISARead = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 50) -join '')
                    $siteID = $ISARead.Substring(35, 15)
                    $siteID = $siteID.Trim();
                    Write-Host "ISA06 of $file is $siteID"
                    move-item -literalpath $file "$destroot\$direction\$siteID"
                    Write-Host "Moved '$file' to '$destroot\$direction\$siteID'"
                }
            }
            else {
                Foreach ($file in $allFiles) {
                    $ISARead = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 70) -join '')
                    $siteID = $ISARead.Substring(54, 15)
                    $siteID = $siteID.Trim();
                    Write-Host "ISA08 of $file is $siteID"
                    move-item -literalpath $file "$destroot\$direction\$siteID"
                    Write-Host "Moved '$file' to '$destroot\$direction\$siteID'"
                }
            }
        }
    }

    Write-Host "X12 sort program starting" -ForegroundColor Cyan

    NonStandardISA
    StandardISA
    
    Write-Host "Script Complete" -ForegroundColor Cyan
}