<#

.SYNOPSIS
    This script simply copies yesterday's files from SI's PreProcess archive for testing.

.DESCRIPTION
    Parameter passed for destination directory.

.POWERSHELL_VERSION 
	V2
	V5

.TABLES
	N/A

.INPUT_FILES
	N/A

.OUTPUT_FILES
	N/A

.RERUN_INSTRUCTIONS
	Can be re-run freely.

.REVISION
	Name: Scott VanderHorst
	Date: 5/15/2020
	Description: Original Code
    
.KNOWN_ISSUES
    N/A

.#>
param(
    [string] $jobNumber = "",
    [string] $destDir
);

if ($null -eq $destDir -or $destDir -eq "") {
    throw "A destination path needs passed, please pass the path the PreProcess archive should be copied to."
};
$destTest = Test-Path "$destDir"
if ($destTest -ne "True") {
    throw "$destDir not found, please troubleshoot."
}

$year = (Get-Date).AddDays(-1).ToString('yyyy')
$month = (Get-Date).AddDays(-1).ToString('MM')
$day = (Get-Date).AddDays(-1).ToString('dd')
$sourceDir = "\\phcsi01\si`$\Archive\In\PreProcess\$year\$month\$day"
$srcTest = Test-Path "$sourceDir"

if ($srcTest -ne "True") {
    throw "$sourceDir not found, please troubleshoot."
}
function copyFiles {
    $counter = 0
    Write-Host "Copying files from $sourceDir to $destDir"
    $dirs = Get-ChildItem $sourceDir -Directory -Exclude "U0000*"
    foreach ($dir in $dirs) {
        $file = Get-ChildItem  "$dir"
        foreach ($f in $file) {
            $datetime = (Get-Date).ToString('yyyyMMdd_HHmmss_fff')
            $filetest = Test-Path "$destDir\$f"
            if ($filetest -eq "True") {
                $extension = [System.IO.Path]::GetExtension($f)
                $name = $f.BaseName
                Copy-Item -literalpath "$dir\$f" "$dir\$name`_$datetime$extension"
                Write-Host "Renamed $name$extension to $name`_$datetime$extension because filename existed in destination"
                $f = "$name`_$datetime$extension"
                Move-Item -literalpath "$dir\$f" "$destDir"
                Write-Host "Copied $f"
            } else {
            Copy-Item -literalpath "$dir\$f" $destDir 
            Write-Host "Copied $f"
        }
            $counter++
        }
    }
    Write-Host "Copied $counter files from $sourceDir to $destDir"
}

copyFiles
