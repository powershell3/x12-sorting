<#

.SYNOPSIS
    This job runs on a scheduled basis to sort X12 files to their proper FTP directories for HealthEdge to filter to Axiom

.DESCRIPTION
    Sorts based on GS08 entries.

.POWERSHELL_VERSION 
	V2
	V5

.TABLES
	N/A

.INPUT_FILES
	N/A

.OUTPUT_FILES
	N/A

.RERUN_INSTRUCTIONS
	Can be re-run freely.

.REVISION
	Name: Scott VanderHorst
	Date: 4/22/2020
	Description: Original Code
    
.KNOWN_ISSUES
    N/A

.#>

$includePath = "$(Split-Path -Path $MyInvocation.MyCommand.Path -Parent | Split-Path -Parent)\include"
. $includePath\utils.ps1

Run-Job -JobName 'X12TSort' {
    $transactionHash = @{ }
    $transactionTypes.Split(';') | ForEach-Object { 
        $key, $value = $_.split(',')
        $transactionHash[$key] = $value    
    }
    $segmentTerms = $segmentTerms.split(';')
    $CRSites = $CRSites.split(';')
    Function ConvertToTest($file) {
        if ($env:COMPUTERNAME -eq $testEnv) {
            Write-Host "Running on $env:COMPUTERNAME, changing ISA15 to T for Axiom"
            $ISATermTestCol = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 106)) -match ":"
            $ISATermTestExc = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 106)) -match "!"
            $ISATermTestGrt = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 106)) -match ">"
            $ISATermTestCol.ToString()
            $ISATermTestExc.ToString()
            $ISATermTestGrt.ToString()
            $ISATermTest = @($ISATermTestCol, $ISATermTestExc, $ISATermTestGrt)
            foreach ($term in $ISATermTest) {
                switch ($term) {
                    ":" {
                        $text = [IO.File]::ReadAllText($file) -replace "\*P\*:", "*T*:"
                        [IO.File]::WriteAllText($file, $text);
                        Write-Host "$file has : terminator, P replaced with T in ISA15"
                        break 
                    }
                    ">" {
                        $text = [IO.File]::ReadAllText($file) -replace "\*P\*>", "*T*>"
                        [IO.File]::WriteAllText($file, $text);
                        Write-Host "$file has > terminator, P replaced with T in ISA15"
                        break 
                    }
                    "!" {
                        $text = [IO.File]::ReadAllText($file) -replace "\*P\*!", "*T*!"
                        [IO.File]::WriteAllText($file, $text);
                        Write-Host "$file has ! terminator, P replaced with T in ISA15"
                        break 
                    }
                    default { break }
                }
            }
        }
        else {
            Write-Host "Running on $env:COMPUTERNAME, no need to change ISA15."
        }
    }
    Function X12File {
        $allFiles = Get-ChildItem "$sourceDir\*"
        if ($allFiles) {
            Foreach ($file in $allFiles) {
                #Make it convert CR to CRLF, the CR in 2089's file is screwing up the get-content
                $ISARead = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 50) -join '')
                $siteID = $ISARead.Substring(35, 15)
                foreach ($site in $CRSites) {
                    if ($siteID.Contains($site)) { 
                        $text = [IO.File]::ReadAllText($file) -replace "`r", "`r`n"
                        [IO.File]::WriteAllText($file, $text)
                    }
                }
                ConvertToTest($file)
                $envelopeRead = ([char[]](Get-Content -literalpath $file -encoding byte -totalcount 250) -join '')
                $GS08 = [regex]::match($envelopeRead, '.*GS\*.*?\*.*?\*.*?\*.*?\*.*?\*.*?\*.*?\*(.*?)[~\*].*').Groups[1].Value
                if (!$GS08) {
                    for ($i = 0; $i -lt $segmentTerms.length; $i++) {
                        $GS08 = [regex]::match($envelopeRead, $segmentTerms[$i]).Groups[1].Value
                        if ($GS08) {
                            break;
                        }
                    }
                }
                foreach ($key in $transactionHash.Keys.GetEnumerator()) {
                    switch ($GS08) {
                        $key {
                            Write-host "'$_' is in the transaction type table"
                            $value = $transactionHash.GetEnumerator() | Where-Object { $_.Key -eq $GS08 } | ForEach-Object { $_.Value }
                            Write-Host "'$_' translates to $value"
                            Move-Item -literalpath $file "$destDir\$value"
                            Write-host "'$file' moved to '$destDir\$value'"
                            break
                        }
                        default { 
                            break 
                        }
                    }
                }
            }
        }
    }
    Function odm277 {
        $allFiles = Get-ChildItem "$sourceDir\*"
        if ($allFiles) {
            Foreach ($file in $allFiles) {
                $extension = [System.IO.Path]::GetExtension($file)
                if ($extension -eq ".277U") {
                    write-host "ODM 277 file. Moving away so it doesn't go to 276 directory. Once the generic task is rerouted this won't be an issue."
                    move-item -literalpath $file "$destDir\ODM277"
                }
            }
        }
    }

    Function isEncrypted {
        $allFiles = Get-ChildItem "$sourceDir\*"
        if ($allFiles) {
            Foreach ($file in $allFiles) {
                $extension = [System.IO.Path]::GetExtension($file)
                if ($extension -eq ".pgp" -or $extension -eq ".gpg") {
                    write-host "$file is encrypted, moving to hold and emailing"
                    move-item -literalpath $file "$destDir\NeedsDecrypted"
                    Send-Email `
                        -Verbose `
                        -AddressDelimiter ';' `
                        -EmailTo $emailTo `
                        -EmailFrom $emailFrom `
                        -EmailSubject $emailSubject `
                        -EmailMessage $emailBody
                }
            }
        }
    }
    Function leftovers {
        $allFiles = Get-ChildItem "$sourceDir\*"
        If ($allFiles) {
            Foreach ($file in $allFiles) {
                move-item -literalpath $file "$destDir\NotX12"
                Write-Host "Moved $file to $destDir\NotX12 because it didn't pass any of the tests in this program."
            }
        }
    }

    Write-Host "X12 sort program starting" -ForegroundColor Cyan

    isEncrypted
    odm277
    X12File
    leftovers
    
    Write-Host "Script Complete" -ForegroundColor Cyan
}